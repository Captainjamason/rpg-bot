// Include FS module
const fs = require('fs');

// Export the test function, if the value isnt 1 then the file wasnt loaded properly
exports.test = function test() {
    return 1;
}

// Load the token file
exports.loadTokens = function loadTokens(callback) {
    fs.readFile('./modules/tokens.json', function(err, data) {
        if(err) {
            console.log("ERROR LOADING TOKENS.JSON: "+err)
        }
        exports.tokens = JSON.parse(data);
        console.log('SUCCESSFULLY LOADED TOKENS');
        callback();
    })
}

// Load the modules
exports.loadModules = function loadModules() {
    fs.readFile('./modules/modules.json', function(err, dat) {
        if(err) {
            console.log("ERROR LOADING MODULES.JSON : " + err)
        }
        let data = JSON.parse(dat);
        /*
            
        */
    })
}
