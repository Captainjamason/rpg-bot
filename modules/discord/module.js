exports.load = function load(callback) {
    try{
        var main = require('./modules/discord/main.js');
        main.start();
        callback();
    } catch(err) {
        return err;
    }
}