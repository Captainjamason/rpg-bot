# DiscoRPG - The modular RPG bot for discord

## Installation
- Clone the git repo or download a release
- run `npm install` to download dependencies
---
## Configuration
- Copy `tokens-temp.json` and rename to `tokens.json`
- Replace tokens with your own
---
## Running
- Run `npm start`