// Init Discord
const discord = require('discord.js');
const dClient = new discord.Client();
// Create file scope variables
let moduleHandler;
let discordToken;

loadModHandler();
moduleHandler.loadTokens(function() {
    discordToken = moduleHandler.tokens.discord;
    connectBot();
});

// Load the module handler
function loadModHandler() {
    console.log('ATTEMPTING LOADING OF MODULEHANDLER');
    try {
        moduleHandler = require('./modules/modulehandler');
        // Use the test value from the module handler
        let testVal = moduleHandler.test();
        if(testVal == '1') {
            console.log('MODULEHANDLER LOADED SUCCESSFULLY');
        }
    } catch (error) {
        console.log('MODULEHANDLER LOADING FAILED '+error);
    }
}

// Connect the bot to discord
function connectBot() {
    console.log('ATTEMPTING TO CONNECT TO DISCORD');
    try {
        dClient.on('ready', () => {
            console.log('SUCCESSFULLY LOGGED INTO DISCORD AS: '+dClient.user.tag);
        })
        dClient.login(discordToken);
    } catch (error) {
        console.log('ERROR LOGGING INTO DISCORD: '+error);
    }
}